<?php
/**
 * @file
 * Configuration form for Akamai Media Viewer.
 */
namespace Drupal\akamai_media_viewer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for all the config variables required for Akamai Media Viewer.
 */
class AkamaiSettingsForm extends ConfigFormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return "akamai_settings";
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [
        'akamai.settings',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config  = $this->config('akamai.settings');

        $form['id'] =[
        '#type' => 'textfield',
        '#title' => 'Enter id',
        '#default_value' => $config->get('id')
        ];

        $form['viewer_type'] = [
        '#type' => 'select',
        '#title' => 'Viewer Type',
        '#options' => [
        'carousel' => $this->t('Carousel'),
        'magnifier' => $this->t('Magnifier'),
        'spin' => $this->t('Spin 360(Not available)'),
        'video' => $this->t('Video(Not available)'),
        'full_screen' => $this->t('Full Screen(Not available)')
        ],
        '#default_value' => $config->get('viewer_type')
        ];

        $form['magnifier']['add_button'] = [
        '#type' => 'checkbox',
        '#title' => 'Buttons',
        '#description' => $this->
          t('Whether to show a button for toggling magnification (default: true)'),
        // '#default_value' => 'selected',
        '#attributes' => ['checked' => 'checked'],
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        '#default_value' => $config->get('add_button')
        ];

        $form['magnifier']['delay'] = [
        '#type' => 'number',
        '#title' => 'Delay',
        '#description' => $this->t(
            'The time delay in milliseconds between mouse hover and
          magnification (breakpoints supported)'
        ),
        '#default_value' => '300',
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        '#default_value' => $config->get('delay')
        ];

        $form['magnifier']['enabled'] = [
        '#type' => 'checkbox',
        '#title' => 'Enable Magnification',
        '#description' => $this->t(
            'Whether to show a button for
          toggling magnification (default: true)'
        ),
        // '#default_value' => 'selected',
        '#attributes' => ['checked' => 'checked'],
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        '#default_value' => $config->get('enabled')
        ];

        $form['magnifier']['flyout_width'] = [
        '#type' => 'number',
        '#title' => 'Flyout Width',
        '#description' => $this->t('Width of the flyout image'),
        '#default_value' => $config->get('flyout_width'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['flyout_height'] = [
        '#type' => 'number',
        '#title' => 'Flyout Height',
        '#description' => $this->t('Height of the flyout image'),
        '#default_value' => $config->get('flyout_height'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['buttonText'] =[
        '#type' => 'textfield',
        '#title' => 'Button Text',
        '#description' => $this->t(
            'Text for the zoom button.
          Also used for its title attribute.
          (default: "Toggle Image Magnification")'
        ),
        '#default_value' => $config->get('buttonText'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['hoverZoomWithoutClick'] = [
        '#type' => 'checkbox',
        '#title' => 'Zoom Without Click',
        '#description' => $this->t(
            'Zoom starts on mouse hover with no click needed
          (default: true; false will require a click to hover-zoom)'
        ),
        '#attributes' => ['checked' => 'checked'],
        '#default_value' => $config->get('hoverZoomWithoutClick'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['placement'] = [
        '#type' => 'select',
        '#title' => 'Placement',
        '#options' => [
        'inline' => $this->t('Inline'),
        'flyoutloupe' => $this->t('flyoutloupe'),
        'flyouttopleft' => $this->t('flyouttopleft'),
        'flyoutbottomleft' => $this->t('flyoutbottomleft'),
        'flyouttopright' => $this->t('flyouttopright'),
        'flyoutbottomright' => $this->t('flyoutbottomright'),
        ],
        '#description' => $this->t('Breakpoints supported'),
        '#default_value' => $config->get('placement'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['magnification'] = [
        '#type' => 'number',
        '#title' => 'Magnification',
        '#description' => $this->t(
            'The scale factor to magnify the image:
           `2`, `3` (default), `4`, `4.5`, etc'
        ),
        '#default_value' =>  $config->get('magnification'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['image_widths'] = [
        '#type' => 'select',
        '#title' => 'Image widths',
        '#options' => [
        '320' => $this->t('320'),
        '640' => $this->t('640'),
        '800' => $this->t('800'),
        '1024' => $this->t('1024'),
        '2048' => $this->t('2048'),
        '5000' => $this->t('5000'),
        ],
        '#description' => $this->t(
            'List of available widths for an image
          (to be combined with image.widthParam)'
        ),
        '#default_value' => $config->get('image_widths'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['image_sizes'] =[
        '#type' => 'textfield',
        '#title' => 'Image Sizes',
        '#description' => $this->t(
            'Value for image sizes attribute.
          Default is set dynamically to viewer width when rendered with JS,
          and updated dynamically.'
        ),
        '#default_value' => $config->get('image_sizes'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['image_policy'] =[
        '#type' => 'textfield',
        '#title' => 'Image Policy',
        '#description' => $this->t(
            'Query param value for policy, 
          appended to &policy= per image url when specified. Values: `foo`.'
        ),
        '#default_value' => $config->get('image_policy'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];

        $form['magnifier']['image_widthparam'] =[
        '#type' => 'textfield',
        '#title' => 'Image Width param',
        '#description' => $this->t(
            'Query string name to use for setting each url width.
          Default urls will be ?imwidth=320 for example.
          Values: `imwidth` (default), `w`, `width`, etc'
        ),
        '#default_value' => $config->get('image_widthparam'),
        '#states' => [
        //show this textfield only if the select 'magnifier' is selected above
        'visible' => [
          ':input[name="viewer_type"]' => ['value' => 'magnifier'],
        ],
        ],
        ];
        return parent::buildForm($form, $form_state);

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);

        $this->config('akamai.settings')
            ->set('id', $form_state->getValue('id'))
            ->set('viewer_type', $form_state->getValue('viewer_type'))
            ->set('add_button', $form_state->getValue('add_button'))
            ->set('delay', $form_state->getValue('delay'))
            ->set('enabled', $form_state->getValue('enabled'))
            ->set('flyout_width', $form_state->getValue('flyout_width'))
            ->set('flyout_height', $form_state->getValue('flyout_height'))
            ->set('buttonText', $form_state->getValue('buttonText'))
            ->set(
                'hoverZoomWithoutClick',
                $form_state->getValue('hoverZoomWithoutClick')
            )
            ->set('placement', $form_state->getValue('placement'))
            ->set('magnification', $form_state->getValue('magnification'))
            ->set('image_widths', $form_state->getValue('image_widths'))
            ->set('image_sizes', $form_state->getValue('image_sizes'))
            ->set('image_policy', $form_state->getValue('image_policy'))
            ->set('image_widthparam', $form_state->getValue('image_widthparam'))
            ->save();
        $this->messenger()->addStatus(
            $this->t('The configuration options have been saved.')
        );
    }
}
